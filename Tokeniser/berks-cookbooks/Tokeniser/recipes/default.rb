#
# Cookbook Name:: test
# Recipe:: default
#
# Copyright (C) 2016 YOUR_NAME
#
# All rights reserved - Do Not Redistribute
#

include_recipe "deploy"
include_recipe "php"
include_recipe "httpd"

package 'apache2' do
  case node[:platform]
  when 'centos','redhat','fedora','amazon'
    package_name 'httpd'
  when 'debian','ubuntu'
    package_name 'apache2'
  end
  action :install
end

node['php']['install_method']

# httpd_service 'default' do
#   action :create
# end

# httpd_config 'tokeniserconfig' do
#   source 'apacheconfig.erb'
#   notifies :restart, 'httpd_service[default]'
# end

node[:deploy].each do |application, deploy|

  Chef::Log.info("Deploying application #{application} on #{node[:opsworks][:instance][:hostname]}")

  opsworks_deploy_dir do
    user deploy[:user]
    group deploy[:group]
    path deploy[:deploy_to]
  end

  opsworks_deploy do
    app application
    deploy_data deploy
  end

  # Chef::Log.info("Running composer update on #{deploy[:deploy_to]}")
  # composer_update do
  #   path deploy[:deploy_to]
  # end
end