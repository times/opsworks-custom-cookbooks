#
# Cookbook Name:: test
# Recipe:: default
#
# Copyright (C) 2016 YOUR_NAME
#
# All rights reserved - Do Not Redistribute
#
include_recipe 'apache2::default'

web_app "tokeniser" do
       template 'apacheconfig.erb'
    end

log 'message' do
  message 'OMG.'
  level :info
end

 # httpd_service 'default' do
 #   action [:create, :start]
 # end

 # httpd_config 'tokeniserconfig' do
 #   source 'apacheconfig.erb'
 #   notifies :restart, 'httpd_service[default]'
 # end

# node[:deploy].each do |application, deploy|

#   Chef::Log.info("Deploying application #{application} on #{node[:opsworks][:instance][:hostname]}")

  # opsworks_deploy_dir do
  #   user deploy[:user]
  #   group deploy[:group]
  #   path deploy[:deploy_to]
  # end

  # opsworks_deploy do
  #   app application
  #   deploy_data deploy
  # end

  # Chef::Log.info("Running composer update on #{deploy[:deploy_to]}")
  # composer_update do
  #   path deploy[:deploy_to]
  # end
# end